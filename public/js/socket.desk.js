var socket = io();

var searchParams = new URLSearchParams(window.location.search);

if (!searchParams.has('escritorio')) {
    window.location = 'index.html';
    throw new Error('El parametro escritorio es necesario.');
}

var desk = searchParams.get('escritorio');


var labelDeskNumber = document.getElementById('desk-number');
var labelServingTicket = document.getElementById('serving-ticket');
var buttonServeTicket = document.getElementById('serve-ticket');


labelDeskNumber.innerText = desk;

buttonServeTicket.addEventListener('click', function(e) {

    socket.emit('serve-ticket', { desk }, function(ticket) {

        if (ticket.err) {

            labelServingTicket.innerText = `[${ticket.message}]`;
            alert(ticket.message);

        } else {

            labelServingTicket.innerText = ticket.number;

        }

    });

});