var socket = io();

var labelTickets = [
    document.getElementById('lblTicket1'),
    document.getElementById('lblTicket2'),
    document.getElementById('lblTicket3'),
    document.getElementById('lblTicket4'),
];

var labelDesks = [
    document.getElementById('lblEscritorio1'),
    document.getElementById('lblEscritorio2'),
    document.getElementById('lblEscritorio3'),
    document.getElementById('lblEscritorio4'),
];

socket.on('current-status', function(data) {

    updateHTML(data.last4);

});

socket.on('last-four', function(data) {

    var audio = new Audio('/audio/new-ticket.mp3');
    audio.play();
    updateHTML(data.last4);

});

function updateHTML(last4) {
    for (let i = 0; i < last4.length; i++) {

        labelTickets[i].innerText = 'Ticket ' + last4[i].number;
        labelDesks[i].innerText = 'Escritorio ' + last4[i].desk;

    }
}