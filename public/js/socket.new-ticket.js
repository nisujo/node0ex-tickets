var socket = io();

let labelTicket = document.getElementById('lblNuevoTicket');
let buttonNext = document.getElementById('new-ticket');

socket.on('connect', function() {
    console.log('Servicio de tickets conectado');
});

socket.on('disconnect', function() {
    console.log('Desconectado del servicio de tickets');
});

socket.on('current-status', function(data) {

    labelTicket.innerText = data.lastTicket;

});

buttonNext.addEventListener('click', function(e) {

    socket.emit('next-ticket', null, function(data) {

        labelTicket.innerText = data;

    });

});