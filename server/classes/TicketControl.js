const fs = require('fs');

class Ticket {

    constructor(number, desk) {
        this.number = number;
        this.desk = desk;
    }
}

class TicketControl {

    constructor() {
        this.last = 0;
        this.today = new Date().getDate();
        this.tickets = [];
        this.last4 = [];

        let data = require('../data/data.json');

        if (data.today == this.today) {

            this.last = data.last;
            this.tickets = data.tickets || [];
            this.last4 = data.last4 || [];

        } else {

            this.resetCount();

        }
    }

    /**
     * Generar un nuevo ticket y ponerlo en la lista de espera.
     */
    next() {
        this.last += 1;

        let ticket = new Ticket(this.last, null);
        this.tickets.push(ticket);

        this.saveFile();

        return `Ticket ${this.last}`;
    }

    getLastTicket() {
        return `Ticket ${this.last}`;
    }

    getLast4() {
        return this.last4;
    }

    serveTicket(desk) {
        if (this.tickets.length == 0) {
            return null;
        }

        // Eliminar el ticket de la lista de espera.
        let ticketNumber = this.tickets[0].number;
        this.tickets.shift();

        // Asignar el ticket a un escritorio y agregarlo al inicio de la cola que está siendo atendida.
        let ticketServe = new Ticket(ticketNumber, desk);
        this.last4.unshift(ticketServe);

        // Si hay mas de 4 se borra el mas antiguo.
        if (this.last4.length > 4) {
            this.last4.pop();
        }

        this.saveFile();

        return ticketServe;
    }

    resetCount() {
        this.tickets = [];
        this.last4 = [];
        this.last = 0;
        this.today = new Date().getDate();
        this.saveFile();
    }

    saveFile() {
        let data = {
            last: this.last,
            today: this.today,
            tickets: this.tickets,
            last4: this.last4
        }

        fs.writeFileSync('./server/data/data.json', JSON.stringify(data));
    }
}


module.exports = {
    TicketControl
}