const { io } = require('../server');
const { TicketControl } = require('../classes/TicketControl');

const ticketControl = new TicketControl();

io.on('connection', (client) => {

    // Generar el siguiente ticket.
    client.on('next-ticket', function(data, callback) {
        let ticket = ticketControl.next();
        callback(ticket);
    });

    // Emitir el estado actual.
    client.emit('current-status', {
        lastTicket: ticketControl.getLastTicket(),
        last4: ticketControl.getLast4()
    });

    // Atender ticket.
    client.on('serve-ticket', function(data, callback) {
        if (!data.desk) return callback({ err: true, message: 'El número del escritorio es obligario.' });

        let ticket = ticketControl.serveTicket(data.desk);

        if (!ticket) return callback({ err: true, message: 'No hay tickets' });

        callback(ticket);

        // Avisar a todos los cuales son los últimos 4 tickets antendidos.
        client.broadcast.emit('last-four', {
            last4: ticketControl.getLast4()
        });
    });

});